var Express = require('express');
var expect = require('expect.js');

var createPermissionChecker = require('./');
var MockRequest = function () { }
var MockResponse = function () { }
var MockNext = function (cb) {
    return function next(error) {
        cb(error);
    }
}

describe('default createPermissionChecker', function () {

    it('should return a permissionChecker to create middlewares when passed nothing', function () {
        expect(createPermissionChecker()).to.be.a('function');
    });

    describe('permissionChecker', function () {
        var checkPermissions = createPermissionChecker();
        it('should return a middleware', function () {
            expect(checkPermissions('read:users')).to.be.a('function');
        });

        describe('Default permission checking middleware', function () {
            var checkReadUsers = checkPermissions('read:users');
            it('should respond with an error if there were no user', function () {
                var req = new MockRequest();
                var res = new MockResponse();
                var next = MockNext(function (error) {
                    expect(error).to.be.a(Error);
                });
                checkReadUsers(req, res, next);
            });

            it('should respond with an error if user had no scope', function () {
                var req = new MockRequest();
                var res = new MockResponse();
                var next = MockNext(function (error) {
                    expect(error).to.be.a(Error);
                });

                req.user = {};
                checkReadUsers(req, res, next);
            });

            it('should respond with an error if user did not have the correct scope', function () {
                var req = new MockRequest();
                var res = new MockResponse();
                var next = MockNext(function (error) {
                    expect(error).to.be.a(Error);
                });

                req.user = { scope: '' };
                checkReadUsers(req, res, next);
            });

            it('should not respond with an error if user had valid scope', function () {
                var req = new MockRequest();
                var res = new MockResponse();
                var next = MockNext(function (error) {
                    expect(error).to.be(undefined);
                });

                req.user = { scope: 'read:users' };
                checkReadUsers(req, res, next);
            });
        });
    });
});


describe('custom permissonChecker', function () {
    it('should return a permissionChecker to create middlewares when passed options', function () {
        expect(createPermissionChecker({
            getPermissionsFromRequest: function noop() { }
        })).to.be.a('function');
    });

    describe('custom permission checker', function () {
        var checkPermission = createPermissionChecker({
            getPermissionsFromRequest: function (req) {
                return req.session ? (req.session.permissions || []) : null;
            }
        });

        it('should return a permissionChecker to create middlewares when passed nothing', function () {
            expect(createPermissionChecker()).to.be.a('function');
        });

        describe('session based checker', function () {

            var checkReadUsers = checkPermission('read:users');

            it('should respond with an error if there were no session', function () {
                var req = new MockRequest();
                var res = new MockResponse();
                var next = MockNext(function (error) {
                    expect(error).to.be.a(Error);
                });
                checkReadUsers(req, res, next);
            });

            it('should respond with an error if user had no valid permissions', function () {
                var req = new MockRequest();
                var res = new MockResponse();
                var next = MockNext(function (error) {
                    expect(error).to.be.a(Error);
                });

                req.session = {};
                checkReadUsers(req, res, next);
            });

            it('should not respond with an error if user had valid permission', function () {
                var req = new MockRequest();
                var res = new MockResponse();
                var next = MockNext(function (error) {
                    expect(error).to.be(undefined);
                });

                req.session = { permissions: ['read:users'] };
                checkReadUsers(req, res, next);
            });
        });

    });
});