# Connect-Ensure-Permission

Similar to connect-ensure-login and you should use `ensure-login` in conjugation with this package. 

## Usage 

### Installation

Simply install this package using npm as 

```
$npm install --save connect-ensure-permission
```


### Ensuring permissions

You can simply add this as a route middleware in any connect friendly library as express or connect in nodejs as follows.

```javascript
import createPermissionChecker from 'connect-ensure-permission';

const checkPermission = createPermissionChecker();

req.get('/foo/bar', checkPermission('foo'), function (req, res){
    res.send('Hurray I can foo');
});
```


By default his package assumes that `req.user.scope` will represent the permissions that the user has. You can however, pass a custom function to extract an array of permissions from the request. As follows.

```javascript
import createPermissionChecker from 'connect-ensure-permission';

const checkPermission = createPermissionChecker(function (req){
    return req.session.permissions;
});

req.get('/foo/bar', checkPermission('foo'), function (req, res) {
    res.send('Hurray I can foo');
});
```

In the above example we are assuming that the permissions were set at a time in the past in the users session.

### Todo in future

- Allow passing array of permissions and multiple permissions in some manner.