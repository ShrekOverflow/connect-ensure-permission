function getPermissionsFromRequestScope(req) {
    var user = req.user;
    if(!user) {
        return null;
    }

    var scope = user.scope;
    if (!scope) {
        return null;
    }

    return scope.split(' ');
}

module.exports = function createPermissionChecker(options) {
    options = options || {};
    options.getPermissionsFromRequest = options.getPermissionsFromRequest || getPermissionsFromRequestScope; 

    return function check(permission) {
        return function ensurePermission(req, res, next) {
            var permissions = options.getPermissionsFromRequest(req);

            if (!permissions) {
                return next(new Error('Could not extract permissions from the request'));
            }

            if (!permissions.includes(permission)){
                return next(new Error('Unauthorized'));
            }
            
            next();
        }
    }
}